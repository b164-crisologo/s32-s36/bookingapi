const User = require("../models/User")
const Course = require("../models/course.js")

const bcrypt = require("bcrypt");
const auth =  require("../auth.js")
//Check if the email already exists
/*
1. use mongoose "find" method to find duplicate emails
2. use the then method to send a responce back to the client


*/ 

module.exports.checkemailExists = (reqBody) => {
	return User.find({email: reqBody.email}).then(result => {
		if(result.length > 0){
			return true;
		} else {
			//No duplicate email found
			//The user is not yet registered in the database
			return false;
		}
	})
}

//User Registration
/*
Steps:
1. Create a new User object
2. Make sure that the password is encrypted.
3. Save the new User to the database

*/
/*
Routes (arguments)			controllers(parameter)
registerUser(req.body, req.params) = > (kahitAnongPangalan)
*/


module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		mobileNo: reqBody.mobileNo,
		email: reqBody.email,
		//10 is the value provided as the number of "salt" rounds that the bcrypt algorithm will run in 
		//in order to encrypt the password
		password: bcrypt.hashSync(reqBody.password, 10)
	})

	//Saves the created object to our database
	return newUser.save().then((user, error) => {
		if(error) {
			return false;
		} else {
			return true
		}
	})
}


//User Authentication
/*
1. Check the database if the user email exists
2. Compare the password provided in the login form with the password
stored in the database.
3. Generate/return a JSON we token if the user is successfully logged in and return false if not
*/

module.exports.loginUser = (reqBody) => {
	//findOne it will returns the first record in the collection that matches the search criteria

	return User.findOne({ email: reqBody.email }).then(result => {
		if(result == null){
			return false;
		} else {
			//User exists    
			//the "compareSync" method is used to compare a non encrypted password from the login form to the 
			//encrypted password retrieved from the database and returns "true" or "false"
			                                             // from Login      from database
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			
			//If the password match
			if(isPasswordCorrect){
				//Generate an access token if true
				return { accessToken : auth.createAccessToken(result.toObject())}
			} else {
				//Password does not match
				return false;
			}


		}
	})
}


//ACTIVITY T-T

//GET all Id
module.exports.getAllId= () => {
	return User.find({}).then( result => {
		return result;
	})
}


//GET Id
module.exports.getProfile= (userId) => {
	return User.findById(userId).then( result => {
		return result;
	})
}




module.exports.modifyPassword = (userId, newContent) => {
	return User.findById(userId).then((result, error) => {
		if(error ){
			console.log(error);
			return false;
		} 

		result.password = " ";

		return result.save().then((updatedPass, saveErr) =>{
			if(saveErr){
				console.log(saveErr);
				return false;
			} else {
				return updatedPass;
			}
	})
})
}

//ACTIVITY SOLUTIONS
/*
Steps:
1. Find the document using the user's ID
2. Reassign the password of the returned document to an empty string
3. return the result back to the client

*/

// module.exports.getProfile = (data) => {
// 	return User.findById(data.id).then(result => {
// 		if(result == null) {
// 			return false
// 		} else {
// 			result.password = "";

// 			return result;
// 		}
// 	})
// }


//Enroll user to a course
/*
1. Find the document in the database using the user's ID
2. Add the courseID  to the user's enrollment array using the push method.
3. Add the userId to the course's enrollees arrays
4. Save the document

*/

//Async and await - allow the processes to wait for each other

module.exports.enroll = async (data) => {
	//Add the courseId to the enrollments array of the user

	let isUserUpdated = await User.findById(data.userId).then(user => {
		//push the course id to enrollments property
		user.enrollments.push({ courseId: data.courseId});

		//Save
		return user.save().then((user, error) => {
			if(error){
				return false;
			} else {
				return true;
			}
		})
	});

	let isCourseUpdated = await Course.findById(data.courseId).then(course => {
		//add the userId in the course's database (enrolless)
		course.enrollees.push({ userId: data.userId });

		return course.save().then((user,error) => {
			if(error){
				return false;
			}else {
				return true;
			}
		})
	});

	if(isUserUpdated && isCourseUpdated){
		//User enrollment succesful
		return true;
	}else {
		//user enrollment failed
		return false;
	}

}




























