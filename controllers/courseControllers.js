const Course = require("../models/course.js");
const auth = require("../auth.js")

//Create a new Course
/*
Steps:
1. Create a new Course object
2. Save to the database
3. error handling


*/

module.exports.addCourse = (reqBody) => {
	console.log(reqBody);
	

	
	//Create a new object
	let newCourse = new Course({
		name: reqBody.course.name,
		description: reqBody.course.description,
		price: reqBody.course.price
	});

	//Saves the created object to our database 
	return newCourse.save().then((course, error)=>{
		//Course creation failed
		if(error){
			return false;
		}else{
			//Course creation successful
			return true;
		}
	})
}


//Retrieving All Courses
module.exports.getAllCourses = () => {
	return Course.find({}).then(result => {
		return result;
	})
}

//Retrieve all ACTIVE courses
module.exports.getAllActive = () => {
	return Course.find({isActive: true}).then(result => {
		return result;

	})
}

//Retrieve specific course

module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams).then(result => {
		return result;
	})
}


//Update a course
/*
Steps:
1. Create variable which will contain the information retrieved from the request body
2. find and update course using the course ID
*/

module.exports.updateCourse = (courseId, reqBody) => {
	//specify the properties of the document to be updated
	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
	};

	//findByIdAndUpdate(id, updatesToBeApplied)
	return Course.findByIdAndUpdate(courseId, updatedCourse).then((course,err) => { 
		//course not updated
		if(err) {
			return false;
		} else {
			//courses updated successfully
			return true;
		}
	})
}



module.exports.updateStatus = (courseId, reqBody) => {
	//specify the properties of the document to be updated
	let updatedStatus = {
		isActive : false,
	};

	//findByIdAndUpdate(id, updatesToBeApplied)
	return Course.findByIdAndUpdate(courseId, updatedStatus).then((course,err) => { 
		//course not updated
		if(error) {
			return false;
		} else {
			//courses updated successfully
			return true;
		}
	})
}