const express = require("express");
const router = express.Router();
const auth = require("../auth.js")

const UserController = require("../controllers/userController")

router.post("/checkEmail", (req, res) => {
	UserController.checkemailExists(req.body).then(result => res.send (result))
});

//Registration for user

router.post("/register", (req, res) => {
	UserController.registerUser(req.body).then(result => res.send(result));
});

//User Authentication(login)
router.post("/login", (req,res) => {
	UserController.loginUser(req.body).then(result => res.send(result))
});

//ACTIVITY S33 T-T

//Get all ID
router.get("/details", (req, res) => {
	UserController.getAllId(req.body).then(result => res.send(result))
})


//Get specific ID
router.get("/:id", (req, res) => {
	UserController.getProfile(req.params.id).then(result => res.send(result))
})

//modify password
router.post("/:id/details", (req,res) => {
	UserController.modifyPassword(req.params.id).then(result => res.send(result))
})


//Activity solution
//router.post("/details", (req, res) =>{
// 	UserController.getProfile(req.body).then(result => res.send(result))
// })
//===================================================================


//The "auth.verify acts as a middleware to ensure that the user is logged in before they can get the details of a user"
router.get("/details", auth.verify, (req, res) => {
	//decode() to retrieve the user information from the token passing the "token" from the request headers as an argument

	const userData = auth.decode(req.headers.authorization)

	UserController.getProfile(userData.id).then(result => res.send(result))
})


//Enrollment to a course
router.post("/enroll", auth.verify, (req, res) => {
	let data = {
		userId : auth.decode(req.headers.authorization).id,
		courseId : req.body.courseId
	}

	UserController.enroll(data).then(result => res.send (result));
	
})


//Updating the Status of the Course
router.put("/:courseId/archive", auth.verify, (req,res) => {
	const data ={
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	if(data.isAdmin){
	CourseController.updateStatus(req.params.courseId, req.body).then(result => res.send(result))
	}else{
		res.send(false)
	}
})


























module.exports = router;