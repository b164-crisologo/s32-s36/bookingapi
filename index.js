//setup dependencies
const express = require('express')
const mongoose = require(`mongoose`)
require('dotenv').config();
const cors = require("cors")
const bcrypt = require("bcrypt")

//routes
const userRoutes = require("./routes/userRoutes");
const courseRoutes = require("./routes/courseRoutes")


//server
const app = express();



//allows all resources/origins to access our backend application
app.use(cors())
app.use(express.json());
app.use(express.urlencoded({extended:true}));

//http://localhost:4000/api/users
app.use("/api/users", userRoutes);
//http://localhost:4000/api/courses
app.use("/api/courses", courseRoutes);


//connect to our MongoDB connection
mongoose.connect(process.env.DB_CONNECTION, {
	useNewUrlParser: true,
	useUnifiedTopology: true
	})

let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"))
db.once("open", () => console.log("We're connected to the cloud database"))
app.listen(process.env.PORT, () => console.log (`Now listening to port ${process.env.PORT}`))
